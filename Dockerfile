FROM golang:1.22-bookworm as build

RUN mkdir -p "/tmp/build"
ADD . /tmp/build

RUN bash /tmp/build/bin/build

FROM debian:bookworm-slim

RUN apt update && apt install ca-certificates -y && apt clean

COPY --from=build /tmp/build/bin/output/prometheus-imap-mailbox-exporter /usr/bin/imap-mailbox-exporter

USER 1000

EXPOSE 9117
CMD ["imap-mailbox-exporter"]
